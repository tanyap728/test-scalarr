import {NgModule} from '@angular/core';
import {ProductsComponent} from './products.component/products.component';
import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [ProductsComponent],
  imports: [SharedModule, CommonModule]
})
export class ProductsModule {
}
