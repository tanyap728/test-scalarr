import {Component, Input, OnInit} from '@angular/core';
import {Book} from '../../../model/model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {

  @Input() book: Book;
  constructor() { }

}
