import { Component, OnInit } from '@angular/core';
import {BooksService} from '../../../services/books.service';
import {Observable} from 'rxjs';
import {Book} from '../../../model/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public productList$: Observable<Book>;
  constructor(private booksService: BooksService) {}


  ngOnInit() {
    this.productList$ = this.booksService.books$;
  }

}
