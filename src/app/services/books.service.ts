import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, publishReplay, retry} from 'rxjs/operators';

@Injectable()
export class BooksService {
  public books$;
  constructor(private http: HttpClient) {
    this.books$ = this.fetchBook()
                      .pipe(publishReplay());
    this.books$.connect();
  }

  private fetchBook(): Observable<any> {
    return this.http.get('https://fakerestapi.azurewebsites.net/api/Books')
      .pipe(
        retry(3),
        catchError(( err, caught) => this.handleError(err))
      );
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    return throwError(error.message);
  }
}
