import {Component, OnInit} from '@angular/core';
import {BooksService} from '../../../services/books.service';
import {Observable} from 'rxjs';
import {Book} from '../../../model/model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public productList$: Observable<Book>;
  constructor(private booksService: BooksService) {}

  ngOnInit() {
    this.productList$ = this.booksService.books$;
  }

}
