export interface Book {
  Description: string;
  Excerpt: string;
  ID: number;
  PageCount: number;
  PublishDate: string;
  Title: string;
}
